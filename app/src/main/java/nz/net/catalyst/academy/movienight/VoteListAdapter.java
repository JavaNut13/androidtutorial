package nz.net.catalyst.academy.movienight;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nz.net.catalyst.academy.movienight.model.Vote;

/**
 * Created by will on 22/12/15.
 */
public class VoteListAdapter extends RecyclerView.Adapter<VoteHolder> {
  private ArrayList<Vote> votes;


  public VoteListAdapter(ArrayList<Vote> votes) {
    this.votes = votes;
  }


  @Override
  public VoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.row_vote, parent, false);

    VoteHolder vh = new VoteHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(VoteHolder holder, int position) {
    holder.update(votes.get(position));
  }


  @Override
  public int getItemCount() {
    return votes.size();
  }

  public void replaceAll(ArrayList<Vote> newVotes) {
    votes = newVotes;
    notifyDataSetChanged();
  }
}

class VoteHolder extends RecyclerView.ViewHolder {
  public TextView nameView;

  public VoteHolder(RelativeLayout rl) {
    super(rl);

    nameView = (TextView) rl.findViewById(R.id.name);
  }

  public void update(Vote vote) {
    nameView.setText(vote.getPerson().getName() + " " + vote.getMovie().getName());
  }
}