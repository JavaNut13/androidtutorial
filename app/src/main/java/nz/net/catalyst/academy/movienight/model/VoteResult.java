package nz.net.catalyst.academy.movienight.model;

import com.google.common.base.MoreObjects;

public class VoteResult {

	public static final String RESULT_OK = "OK";
	public static final String RESULT_ERROR = "ERROR";
	
	private String result;
	private String message;
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
    @Override
    public String toString() {
        return toDebugString();
    }

    public String toDebugString() {

        return MoreObjects.toStringHelper(this)
                .add("result", result)
                .add("message", message)
                .toString();
    }
}
