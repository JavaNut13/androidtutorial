package nz.net.catalyst.academy.movienight;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import nz.net.catalyst.academy.movienight.model.Movie;
import nz.net.catalyst.academy.movienight.model.Person;
import nz.net.catalyst.academy.movienight.model.VoteResult;


/**
 * A simple Android activity providing a user interface to the Movie Night web service.
 * All interactions with the web service are encapsulated within the MovieNightService class. It
 * handles all the JSON and http, so the rest of the application deals only with Java objects.
 */
public class MovieNightVoteActivity extends ActionBarActivity {

    // TODO Set this as the URL of your computer
    public static final String SERVER_URL = "http://10.22.32.223:8000";

    static final String TAG = "MovieNightVoteActivity";

    MovieNightService service;

    Spinner userSpinner;
    RadioGroup movieRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_night_vote);

        // disable strict mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        userSpinner = (Spinner) findViewById(R.id.spinner);

        try {

            service = new MovieNightService(SERVER_URL);

            List<Person> persons = service.getUsers();

            ArrayAdapter<Person> adapter = new ArrayAdapter<Person>(this, android.R.layout.simple_spinner_item, persons);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            userSpinner.setAdapter(adapter);

            initialiseMovieRadioGroup();

        } catch (MalformedURLException e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Initialises the radio button group widget to choose a movie from
     * @throws IOException
     */
    void initialiseMovieRadioGroup() throws IOException{
        // Get the group of radio buttons from the layout
        movieRadioGroup = (RadioGroup) findViewById(R.id.movie_radio_group);
        movieRadioGroup.removeAllViews();

        // List of movies to vote to
        List<Movie> movies = service.getMovies();

        // Configures the radio buttons
        for (Movie movie : movies) {
            RadioButton rb = new RadioButton(this);
            rb.setTag(movie);
            rb.setText(movie.getName());
            movieRadioGroup.addView(rb);
        }
    }

    public void submitVote(View view) {

        Person person = getSelectedUser();
        Movie movie = getSelectedMovie();

        if (person == null) {
            Toast.makeText(this, "Please select a person", Toast.LENGTH_SHORT).show();
            return;
        }

        if (movie == null) {
            Toast.makeText(this, "Please select a movie", Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(this, person.getName() + " votes for " + movie.getName(), Toast.LENGTH_SHORT).show();

        try {
            VoteResult result = service.submitVote(getSelectedUser(), getSelectedMovie());

            if (VoteResult.RESULT_OK.equals(result.getResult())) {
                Toast.makeText(this, result.getMessage(), Toast.LENGTH_SHORT).show();
            } else if (VoteResult.RESULT_ERROR.equals(result.getResult())) {
                Toast.makeText(this, "Unable to cast vote: " + result.getMessage(),
                    Toast.LENGTH_SHORT).show();
            } else {
                // something weird is happening. Log it and show the person a generic error message
                Log.e(TAG, "Got unexpected response from server: " + result.toDebugString());
                Toast.makeText(this, "Error submitting vote. The vote hasn't been recorded.",
                    Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            Log.e(TAG, "I/O error submitting vote: " + e.getMessage());
            Toast.makeText(this, "Error submitting vote. The vote hasn't been recorded.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void viewResults(View view) {
        Intent myIntent = new Intent(this, VoteResultsActivity.class);
        startActivity(myIntent);
    }

    public Person getSelectedUser() {
        // That's easy, isn't it?
        return (Person) userSpinner.getSelectedItem();
    }

    public Movie getSelectedMovie() {

        int checkedId = movieRadioGroup.getCheckedRadioButtonId();

        // checkedId will be -1 if no movie has been selected
        if (checkedId == -1) {
            return null;
        }

        RadioButton checkedRadio = (RadioButton) findViewById(checkedId);
        return (Movie) checkedRadio.getTag();
    }
}
