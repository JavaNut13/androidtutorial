package nz.net.catalyst.academy.movienight;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import nz.net.catalyst.academy.movienight.model.Vote;


public class VoteResultsActivity extends ActionBarActivity {

    private RecyclerView voteList = null;
    private MovieNightService service;
    private VoteListAdapter voteArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote_results);
        voteList = (RecyclerView) findViewById(R.id.vote_list);
        voteList.setHasFixedSize(true);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        voteList.setLayoutManager(layoutManager);

        try {
            service = new MovieNightService(MovieNightVoteActivity.SERVER_URL);

            ArrayList<Vote> votes = (ArrayList<Vote>) service.getVotes();

            voteArrayAdapter = new VoteListAdapter(votes);
            voteList.setAdapter(voteArrayAdapter);
        } catch (MalformedURLException e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}

