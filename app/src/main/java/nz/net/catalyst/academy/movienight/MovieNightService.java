package nz.net.catalyst.academy.movienight;

import com.google.common.io.CharStreams;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import nz.net.catalyst.academy.movienight.model.Movie;
import nz.net.catalyst.academy.movienight.model.Person;
import nz.net.catalyst.academy.movienight.model.Vote;
import nz.net.catalyst.academy.movienight.model.VoteResult;


/**
 * A service class that handles all interactions with the Movie Night web service.
 */
public class MovieNightService {
    
    private URL serverUrl;
	
    // Main class for the Gson library
    private Gson gson;
    
    public MovieNightService(String serverUrl) throws MalformedURLException {
        
    	this.serverUrl = new URL(serverUrl);
    	
        this.gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting()
            .create();
    }
    
    public List<Movie> getMovies() throws IOException {

        URL url = new URL(serverUrl, "movies");
        
        JsonObject jsonObj = getJson(url);
        JsonArray jsonMovies = jsonObj.get("movies").getAsJsonArray();
        
        List<Movie> movies = new ArrayList<Movie>();
        
        for (JsonElement jsonMovie : jsonMovies) {
            Movie movie = gson.fromJson(jsonMovie, Movie.class);
            movies.add(movie);
        }
        
        return movies;
    }
    
    public List<Person> getUsers() throws IOException {
        
        URL url = new URL(serverUrl, "people");
        
        JsonObject jsonObj = getJson(url);
        JsonArray jsonMovies = jsonObj.get("people").getAsJsonArray();
        
        List<Person> people = new ArrayList<Person>();
        
        for (JsonElement jsonMovie : jsonMovies) {
            Person person = gson.fromJson(jsonMovie, Person.class);
            people.add(person);
        }
        
        return people;
    }

    public List<Vote> getVotes() throws IOException {

        URL url = new URL(serverUrl, "votes");

        JsonObject jsonObj = getJson(url);
        JsonArray jsonMovies = jsonObj.get("votes").getAsJsonArray();

        List<Vote> votes = new ArrayList<Vote>();

        for (JsonElement jsonMovie : jsonMovies) {
            Vote vote = gson.fromJson(jsonMovie, Vote.class);
            votes.add(vote);
        }

        return votes;
    }
    
    public VoteResult submitVote(Person person, Movie movie) throws IOException {

        URL url = new URL(serverUrl, "vote/" + person.getId() + "/" + movie.getId());
    	
        JsonObject response = post(url);
        
        return gson.fromJson(response, VoteResult.class);
    }
    
    private JsonObject getJson(URL url) throws IOException {
        
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
        String responseStr = CharStreams.toString(reader);  // closes the reader for us

        JsonParser parser = new JsonParser();
        return parser.parse(responseStr).getAsJsonObject();
    }

    private JsonObject post(URL url) throws IOException {

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");

        Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String responseStr = CharStreams.toString(reader);  // closes the reader for us

        JsonParser parser = new JsonParser();
        return parser.parse(responseStr).getAsJsonObject();
    }
}
