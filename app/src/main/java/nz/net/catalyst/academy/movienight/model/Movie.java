package nz.net.catalyst.academy.movienight.model;

import com.google.common.base.MoreObjects;

public class Movie {

    private int id;
    private String name;
    private int length;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        
        return name;
    }

    public String toDebugString() {

        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("length", length)
                .toString();
    }
}
